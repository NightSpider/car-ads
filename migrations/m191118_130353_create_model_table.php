<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%model}}`.
 */
class m191118_130353_create_model_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%model}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer(),
            'name' => $this->string(100)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%model}}');
    }
}
