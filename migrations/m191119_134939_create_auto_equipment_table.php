<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auto_equipment}}`.
 */
class m191119_134939_create_auto_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auto_equipment}}', [
            'id' => $this->primaryKey(),
            'id_auto' => $this->integer(),
            'id_equipment' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%auto_equipment}}');
    }
}
