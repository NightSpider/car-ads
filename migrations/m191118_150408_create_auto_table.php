<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auto}}`.
 */
class m191118_150408_create_auto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auto}}', [
            'id' => $this->primaryKey(),
            'id_brand' => $this->integer(),
            'id_model' => $this->integer(),
            'mileage' => $this->integer(),
            'price' => $this->decimal(),
            'phone' => $this->string(15),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%auto}}');
    }
}
