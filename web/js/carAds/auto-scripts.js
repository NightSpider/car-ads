$(document).ready(function () {

    $(".brand-list").change(function () {

        var params = {
            'id': $(this).val()
        };

        $.post('/carAds/auto/get-model', params, function (data) {
            $(".model-list").empty();
            $.each(data, function(i, p) {
                $(".model-list").append($('<option></option>').val(i).html(p));
            });
        });
    });

    $(".delete-ads").click(function () {
        var params = {
            'id': $(this).attr("data-id")
        };

        $.post('/carAds/auto/delete', params, function () {
            location.reload();
        });
    });
});