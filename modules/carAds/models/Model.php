<?php

namespace app\modules\carAds\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "model".
 *
 * @property int $id
 * @property int $id_brand
 * @property string $name
 */
class Model extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_brand'], 'required'],
            [['id_brand'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Id Brand',
            'name' => 'Name',
        ];
    }

    public function getAllBrands()
    {
        $brands = Brand::find()->asArray()->all();
        $brands = ArrayHelper::map($brands, 'id', 'name');
        return $brands;
    }
    public function getBrands()
    {
        return $this->hasOne(Brand::className(), ['id' => 'id_brand']);
    }
}
