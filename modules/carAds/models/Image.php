<?php

namespace app\modules\carAds\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property int $id_auto
 * @property string $image
 * @property string $image_720x540
 * @property string $image_146x106
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_auto'], 'integer'],
            [['image', 'image_720x540', 'image_146x106'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_auto' => 'Id Auto',
            'image' => 'Image',
            'image_720x540' => 'Image 720x540',
            'image_146x106' => 'Image 146x106',
        ];
    }

    public function getAutos()
    {
        return $this->hasOne(Auto::className(), ['id' => 'id_auto']);
    }
}
