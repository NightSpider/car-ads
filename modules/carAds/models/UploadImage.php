<?php


namespace app\modules\carAds\models;

use Imagine\Image\ImageInterface;
use Yii;
use yii\base\Model;
use yii\imagine\Image;

class UploadImage extends Model
{
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 3],
        ];
    }

    /**
     * @return array|bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $pictures = array();
            foreach ($this->imageFiles as $file) {
                $images = array();
                $images['image_720x540'] = $this->saveResizeImage($file, '720', '540');
                $images['image_146x106'] = $this->saveResizeImage($file, '146', '106');
                $images['image'] = $file->baseName . '.' . $file->extension;

                $file->saveAs(Yii::getAlias('@webroot') . '/images/uploads/' . $file->baseName . '.' . $file->extension);
                $pictures[] = $images;
            }
            return $pictures;
        } else {
            return false;
        }
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @return string
     */
    public function saveResizeImage($file, $width, $height)
    {
        $size = $width . 'x' . $height;

        $filename = $size . '/' . $file->baseName . '-' . $size . '.' . $file->extension;

        Image::thumbnail($file->tempName, $width, $height, ImageInterface::THUMBNAIL_INSET)
            ->save(Yii::getAlias('@webroot') . '/images/uploads/' . $filename, ['quality' => 80]);

        return $filename;
    }

    /**
     * @param $model
     * @return mixed
     */
    static public function getImages($model)
    {
        return $model->getImages()->select('image, image_720x540, image_146x106')->asarray()->all();
    }

    /**
     * @param $images array
     */
    static public function deleteImage($images)
    {
        foreach ($images as $img){
            foreach ($img as $item){
                if(file_exists(Yii::getAlias('@webroot') . '/images/uploads/' . $item)){
                    unlink(Yii::getAlias('@webroot') . '/images/uploads/' . $item);
                }
            }
        }
    }
}