<?php

namespace app\modules\carAds\models;

use Yii;
use yii\helpers\ArrayHelper;;

/**
 * This is the model class for table "auto".
 *
 * @property int $id
 * @property int $id_brand
 * @property int $id_model
 * @property int $mileage
 * @property string $price
 * @property string $phone
 */
class Auto extends \yii\db\ActiveRecord
{
    public $images;
    public $equipmentsList;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['equipmentsList'], 'safe'],
            [['id_brand', 'id_model', 'price', 'phone'], 'required'],
            [['id_brand', 'id_model', 'mileage'], 'integer'],
            [['price'], 'number'],
            [['phone'], 'string', 'max' => 15],
            [['images'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_brand' => 'Brand',
            'id_model' => 'Model',
            'mileage' => 'Mileage',
            'price' => 'Price',
            'phone' => 'Phone',
        ];
    }

    /**
     * Saves a image to a image table
     * @param $imagesList
     * @return bool
     */
    public function saveImage($imagesList)
    {
        foreach ($imagesList as $img){
            $picture = new Image();
            $picture->image = $img['image'];
            $picture->image_146x106 = $img['image_146x106'];
            $picture->image_720x540 = $img['image_720x540'];
            $picture->link('autos', $this);
        }
        return true;
    }

    /**
     * Keeps the connection of equipment with the car
     * @return bool
     */
    public function saveEquipment()
    {
        foreach ($this->equipmentsList as $id){
            $equipment = Equipment::findOne($id);
            $this->link('equipments', $equipment);
        }
        return true;
    }

    public function getAllBrands()
    {
        $brands = Brand::find()->asArray()->all();
        $brands = ArrayHelper::map($brands, 'id', 'name');

        return $brands;
    }

    public function getBrands()
    {
        return $this->hasOne(Brand::className(), ['id' => 'id_brand']);
    }

    public function getModels()
    {
        return $this->hasOne(Model::className(), ['id' => 'id_model']);
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['id_auto' => 'id']);
    }
    public function getEquipments()
    {
        return $this->hasMany(Equipment::className(), ['id' => 'id_equipment'])
            ->viaTable('auto_equipment', ['id_auto' => 'id']);
    }

    public function getAllEquipments()
    {
        $equipments = Equipment::find()->all();
        return ArrayHelper::map($equipments, 'id', 'name');
    }
}
