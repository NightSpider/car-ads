<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\carAds\models\Auto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_brand')->dropDownList($model->getAllBrands(), ['prompt' => 'Choose a brand', 'class' => 'form-control brand-list']) ?>

    <?= $form->field($model, 'id_model')->dropDownList(array(), ['prompt' => 'Choose a brand', 'class' => 'form-control model-list']) ?>

    <?= $form->field($model, 'mileage')->textInput() ?>

    <?= $form->field($model, 'equipmentsList[]')->checkboxList($model->getAllEquipments()) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'images[]')->fileInput(['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
