<?php

use app\modules\carAds\models\UploadImage;
use yii\helpers\Html;

?>
<?php
$images = UploadImage::getImages($model);

foreach ($images as $img) {
    foreach ($img as $item) {

    }
}
?>
<div class="container">
    <div class="row block-view">
        <div class="col-md-8">
            <?php
            $img = $model->getImages()->limit('1')->one();
            ?>
            <?php echo Html::img('@web/images/uploads/' . $img->image_720x540); ?>
            <div class="block-mini-img">
                <?php
                $images = $model->getImages()->all();
                ?>
                <?php foreach ($images as $img) : ?>
                    <?php echo Html::img('@web/images/uploads/' . $img->image_146x106) ?>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-4">
            <h3><?php echo $model->brands->name . ' ' . $model->models->name ?></h3>
            <hr>
            <p><b>Пробег:</b> <?php echo number_format($model->mileage, 0, '', ' '); ?> км</p>
            <hr>
            <p><b>Дополнительное оборудование:</b></p>
            <ul>
                <?php foreach ($model->equipments as $item) : ?>
                    <li><?php echo $item->name; ?></li>
                <?php endforeach; ?>
            </ul>
            <hr>
            <p><b>Цена: </b><?php echo number_format($model->price, 0, '', ' ') ?> рублей</p>
            <hr>
            <p><b>Телефон: </b><?php echo $model->phone ?></p>
        </div>
    </div>
</div>
