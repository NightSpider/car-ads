<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="carAds-default-index">
    <a class="btn btn-info" href="/carAds/brand">Brands</a>
    <a class="btn btn-info" href="/carAds/model">Models</a>
    <a class="btn btn-info" href="/carAds/equipment">Equipment</a>
    <?= Html::a('Create Auto', Url::to(['/carAds/auto/create']), ['class' => 'btn btn-success']) ?>
</div>

<div class="container">
    <?php foreach ($ads as $model) : ?>
        <div class="row ads-block">
            <div class="col-md-3 ">
                <?php
                $image = $model->getImages()->limit('1')->one();
                ?>
                <?= Html::img('@web/images/uploads/' . $image->image_146x106); ?>
            </div>
            <div class="col-md-3 v-center">
                <a href="<?php echo Url::toRoute(['default/view', 'id' => $model->id]) ?>"><?php echo $model->brands->name . ' ' . $model->models->name; ?></a>
            </div>
            <div class="col-md-3 v-center">
                <p><?php echo number_format($model->price, 0, '', ' '); ?> рублей</p>
            </div>
        </div>
        <a class="btn btn-danger delete-ads" data-id="<?php echo $model->id; ?>">Delete</a>
    <?php endforeach; ?>
</div>
<?php
    echo \yii\widgets\LinkPager::widget([
        'pagination' => $pages,
    ]);
?>
