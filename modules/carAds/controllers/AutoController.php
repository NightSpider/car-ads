<?php

namespace app\modules\carAds\controllers;

use app\modules\carAds\models\Brand;
use app\modules\carAds\models\Equipment;
use app\modules\carAds\models\UploadImage;
use Yii;
use app\modules\carAds\models\Auto;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * AutoController implements the CRUD actions for Auto model.
 */
class AutoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Auto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auto();
        $imageForSave = new UploadImage();

        if ($model->load(Yii::$app->request->post())) {

            $imageForSave->imageFiles = UploadedFile::getInstances($model, 'images');

            if($imagesList = $imageForSave->upload()){
                if($model->save()){

                    $model->saveImage($imagesList);

                    $model->saveEquipment();

                    return $this->redirect(['/carAds/default/index']);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Auto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');

            $images = UploadImage::getImages($this->findModel($id));
            UploadImage::deleteImage($images);

            $this->findModel($id)->delete();

            return true;
        }

        return false;
    }

    /**
     * Finds the Auto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auto::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Returns models for the brand
     * @return array|bool
     */
    public function actionGetModel()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(Yii::$app->request->isAjax){
            $brand = Brand::findOne(Yii::$app->request->post('id'));
            return ArrayHelper::map($brand->models, 'id', 'name');
        }

        return false;
    }
}
