<?php

namespace app\modules\carAds\controllers;

use app\modules\carAds\models\Auto;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Default controller for the `carAds` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $models = Auto::find();
        $countModel = clone $models;

        $pages = new Pagination([
            'totalCount' => $countModel->count(),
            'pageSize' => 5,
            'defaultPageSize' => 5,
        ]);

        $ads = $models->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', [
            'ads' => $ads,
            'pages' => $pages,
        ]);
    }

    public function actionView($id)
    {
        $model = Auto::findOne($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }
}
