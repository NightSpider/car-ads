-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 20 2019 г., 10:50
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2basic`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auto`
--

CREATE TABLE `auto` (
  `id` int(11) NOT NULL,
  `id_brand` int(11) DEFAULT NULL,
  `id_model` int(11) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auto`
--

INSERT INTO `auto` (`id`, `id_brand`, `id_model`, `mileage`, `price`, `phone`) VALUES
(1, 4, 3, 10000, '740000', '8(962) 408-70-2'),
(2, 5, 4, 0, '2086600', '8(879) 332-32-3'),
(3, 1, 5, 147000, '1270000', '8(918) 085-68-0'),
(4, 6, 6, 0, '587000', '8(879) 331-76-3'),
(5, 7, 7, 147000, '1390000', '8(863) 255-85-0'),
(6, 8, 8, 0, '2659900', '8(863) 255-85-0');

-- --------------------------------------------------------

--
-- Структура таблицы `auto_equipment`
--

CREATE TABLE `auto_equipment` (
  `id` int(11) NOT NULL,
  `id_auto` int(11) DEFAULT NULL,
  `id_equipment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auto_equipment`
--

INSERT INTO `auto_equipment` (`id`, `id_auto`, `id_equipment`) VALUES
(6, 1, 4),
(7, 1, 5),
(8, 1, 6),
(9, 2, 1),
(10, 2, 2),
(11, 2, 3),
(12, 2, 4),
(13, 2, 5),
(14, 2, 6),
(15, 3, 1),
(16, 3, 2),
(17, 4, 3),
(18, 4, 4),
(19, 4, 5),
(20, 5, 1),
(21, 5, 2),
(22, 5, 3),
(23, 5, 4),
(24, 5, 5),
(25, 5, 6),
(26, 6, 1),
(27, 6, 2),
(28, 6, 3),
(29, 6, 4),
(30, 6, 5),
(31, 6, 6),
(32, 7, 1),
(33, 7, 2),
(34, 7, 3),
(35, 8, 1),
(36, 8, 2),
(37, 8, 3),
(38, 9, 1),
(39, 9, 2),
(40, 9, 3),
(41, 10, 1),
(42, 10, 2),
(43, 10, 3),
(44, 10, 4),
(45, 10, 5),
(46, 11, 1),
(47, 11, 2),
(48, 11, 3),
(49, 11, 4),
(50, 12, 1),
(51, 12, 2),
(52, 12, 3),
(53, 12, 4),
(54, 12, 5),
(55, 12, 6),
(56, 13, 1),
(57, 13, 2),
(58, 13, 3),
(59, 14, 1),
(60, 14, 2),
(61, 14, 3),
(62, 15, 1),
(63, 15, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`id`, `name`) VALUES
(1, 'Audi'),
(4, 'Jac'),
(5, 'Skoda'),
(6, 'ВАЗ (LADA)'),
(7, 'Toyota'),
(8, 'Subaru');

-- --------------------------------------------------------

--
-- Структура таблицы `equipment`
--

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `equipment`
--

INSERT INTO `equipment` (`id`, `name`) VALUES
(1, 'Подушка безопасности водителя'),
(2, 'Подушки безопасности боковые'),
(3, 'Центральный замок'),
(4, 'Легкосплавные диски'),
(5, 'Кондиционер'),
(6, 'ABS');

-- --------------------------------------------------------

--
-- Структура таблицы `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `id_auto` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_720x540` varchar(100) DEFAULT NULL,
  `image_146x106` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `image`
--

INSERT INTO `image` (`id`, `id_auto`, `image`, `image_720x540`, `image_146x106`) VALUES
(45, 1, 'js2.jpg', '720x540/js2-720x540.jpg', '146x106/js2-146x106.jpg'),
(46, 1, 'js3.jpg', '720x540/js3-720x540.jpg', '146x106/js3-146x106.jpg'),
(47, 1, 'js5.jpg', '720x540/js5-720x540.jpg', '146x106/js5-146x106.jpg'),
(48, 2, 'skoda1.jpg', '720x540/skoda1-720x540.jpg', '146x106/skoda1-146x106.jpg'),
(49, 2, 'skoda2.jpg', '720x540/skoda2-720x540.jpg', '146x106/skoda2-146x106.jpg'),
(50, 2, 'skoda3.jpg', '720x540/skoda3-720x540.jpg', '146x106/skoda3-146x106.jpg'),
(51, 3, 'q5_1.jpg', '720x540/q5_1-720x540.jpg', '146x106/q5_1-146x106.jpg'),
(52, 3, 'q5_2.jpg', '720x540/q5_2-720x540.jpg', '146x106/q5_2-146x106.jpg'),
(53, 3, 'q5_3.jpg', '720x540/q5_3-720x540.jpg', '146x106/q5_3-146x106.jpg'),
(54, 4, 'lada1.jpg', '720x540/lada1-720x540.jpg', '146x106/lada1-146x106.jpg'),
(55, 4, 'lada2.jpg', '720x540/lada2-720x540.jpg', '146x106/lada2-146x106.jpg'),
(56, 4, 'lada3.jpg', '720x540/lada3-720x540.jpg', '146x106/lada3-146x106.jpg'),
(57, 5, 'toy1.jpg', '720x540/toy1-720x540.jpg', '146x106/toy1-146x106.jpg'),
(58, 5, 'toy2.jpg', '720x540/toy2-720x540.jpg', '146x106/toy2-146x106.jpg'),
(59, 5, 'toy3.jpg', '720x540/toy3-720x540.jpg', '146x106/toy3-146x106.jpg'),
(60, 6, 'sub1.jpg', '720x540/sub1-720x540.jpg', '146x106/sub1-146x106.jpg'),
(61, 6, 'sub2.jpg', '720x540/sub2-720x540.jpg', '146x106/sub2-146x106.jpg'),
(62, 6, 'sub3.jpg', '720x540/sub3-720x540.jpg', '146x106/sub3-146x106.jpg'),
(63, 7, 'q5_1.jpg', '720x540/q5_1-720x540.jpg', '146x106/q5_1-146x106.jpg'),
(64, 7, 'q5_2.jpg', '720x540/q5_2-720x540.jpg', '146x106/q5_2-146x106.jpg'),
(65, 7, 'q5_3.jpg', '720x540/q5_3-720x540.jpg', '146x106/q5_3-146x106.jpg'),
(66, 8, 'q5_1.jpg', '720x540/q5_1-720x540.jpg', '146x106/q5_1-146x106.jpg'),
(67, 8, 'q5_2.jpg', '720x540/q5_2-720x540.jpg', '146x106/q5_2-146x106.jpg'),
(68, 8, 'q5_3.jpg', '720x540/q5_3-720x540.jpg', '146x106/q5_3-146x106.jpg'),
(69, 9, 'q5_1.jpg', '720x540/q5_1-720x540.jpg', '146x106/q5_1-146x106.jpg'),
(70, 9, 'q5_2.jpg', '720x540/q5_2-720x540.jpg', '146x106/q5_2-146x106.jpg'),
(71, 9, 'q5_3.jpg', '720x540/q5_3-720x540.jpg', '146x106/q5_3-146x106.jpg'),
(72, 10, 'auto_1.jpg', '720x540/auto_1-720x540.jpg', '146x106/auto_1-146x106.jpg'),
(73, 10, 'auto_3.jpg', '720x540/auto_3-720x540.jpg', '146x106/auto_3-146x106.jpg'),
(74, 10, 'avto_2.jpg', '720x540/avto_2-720x540.jpg', '146x106/avto_2-146x106.jpg'),
(75, 11, 'auto_1.jpg', '720x540/auto_1-720x540.jpg', '146x106/auto_1-146x106.jpg'),
(76, 11, 'auto_3.jpg', '720x540/auto_3-720x540.jpg', '146x106/auto_3-146x106.jpg'),
(77, 11, 'avto_2.jpg', '720x540/avto_2-720x540.jpg', '146x106/avto_2-146x106.jpg'),
(78, 12, 'q5_1.jpg', '720x540/q5_1-720x540.jpg', '146x106/q5_1-146x106.jpg'),
(79, 12, 'q5_2.jpg', '720x540/q5_2-720x540.jpg', '146x106/q5_2-146x106.jpg'),
(80, 12, 'q5_3.jpg', '720x540/q5_3-720x540.jpg', '146x106/q5_3-146x106.jpg'),
(81, 13, 'auto_1.jpg', '720x540/auto_1-720x540.jpg', '146x106/auto_1-146x106.jpg'),
(82, 14, 'auto_1.jpg', '720x540/auto_1-720x540.jpg', '146x106/auto_1-146x106.jpg'),
(83, 14, 'auto_3.jpg', '720x540/auto_3-720x540.jpg', '146x106/auto_3-146x106.jpg'),
(84, 15, 'auto_1.jpg', '720x540/auto_1-720x540.jpg', '146x106/auto_1-146x106.jpg'),
(85, 15, 'auto_3.jpg', '720x540/auto_3-720x540.jpg', '146x106/auto_3-146x106.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1574078378),
('m191118_115835_create_brand_table', 1574078381),
('m191118_130353_create_model_table', 1574082338),
('m191118_150408_create_auto_table', 1574089674),
('m191119_113807_create_image_table', 1574163503),
('m191119_134233_create_equipment_table', 1574170996),
('m191119_134939_create_auto_equipment_table', 1574171445);

-- --------------------------------------------------------

--
-- Структура таблицы `model`
--

CREATE TABLE `model` (
  `id` int(11) NOT NULL,
  `id_brand` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `model`
--

INSERT INTO `model` (`id`, `id_brand`, `name`) VALUES
(3, 4, 'S5'),
(4, 5, 'Superb'),
(5, 1, 'Q5'),
(6, 6, 'Granta'),
(7, 7, 'Rav 4'),
(8, 8, 'Forester');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `auto_equipment`
--
ALTER TABLE `auto_equipment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `auto`
--
ALTER TABLE `auto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `auto_equipment`
--
ALTER TABLE `auto_equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT для таблицы `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT для таблицы `model`
--
ALTER TABLE `model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
